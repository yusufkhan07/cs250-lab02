#include <iostream>
#include <vector>
#include <ctime>
#include <algorithm> 
#include <time.h>

using namespace std;

bool updateTopScores(const vector <int> &, vector <int> &);
bool testUpdateTopScore();

void printVec(const vector <int> &);

bool myCompare(int i, int j) { return i > j; } //Compare function for Sort() to sort in descending order


int main() {

	vector <int> sortedScores; //Sorted Scores

	vector <int> topScores = { 443, 439, 438, 438, 434, 418, 418, 417, 413, 411 };
	topScores.push_back(444); //Pushing a new value

	updateTopScores(topScores, sortedScores); //Sorting Score

	//Calling the test funciton
	if (testUpdateTopScore())
		cout << endl << endl << "Test Case is working" << endl;
	else
		cout << endl << endl << "Test Case isn't working" << endl;


	system("pause");
	return 0;
}


void printVec(const vector <int> &x) {
	for (int i = 0; i < x.size(); i++)
		cout << x[i] << " ";
}

bool updateTopScores(const vector<int> &in, vector<int> & out) {

	out = in;

	for (int i = 0; i < out.size() - 1; i++)
		for (int j = 0; j < out.size() - i - 1; j++)
			if (out[j] < out[j + 1])
				swap(out[j], out[j + 1]);


	return true;
}

bool testUpdateTopScore() {

	//Seed
	srand((int)time(NULL));

	//Declaring Vectors
	vector <int> myarray;
	vector <int> sortedArray;

	//Pushing Random Values into Vector
	for (int i = 0; i < 10; i++)
		myarray.push_back(rand() % 100 + 1);

	//Print Orignal Random Array
	cout << "Orignal Array \t";
	printVec(myarray);

	//Sorted using sort()
	sort(myarray.begin(), myarray.end(), myCompare);

	//Printing output
	cout << endl << endl << "Sorted Array \t";
	printVec(myarray);

	//Adding a new random value
	myarray.push_back(rand() % 100 + 1);

	//Printing output
	cout << endl << endl << "Added new Element at end \t";
	printVec(myarray);

	//Using updateTopScores()
	updateTopScores(myarray, sortedArray);

	//Printing output
	cout << endl << endl << "Sorted using updateTopScores() \t";
	printVec(sortedArray);

	//Sorting using sort() function again
	sort(myarray.begin(), myarray.end(), myCompare);

	//Printing output
	cout << endl << endl << "Sorted using Sort() \t";
	printVec(myarray);

	//Comparing if our sort function and built in sort function output has the same result
	return myarray == sortedArray;

}
